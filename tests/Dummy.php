<?php namespace Soundrussian\Ancestry\Tests;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Dummy extends Eloquent {

  public $timestamps = false;

  public $ancestry_id    = 'url';
  public $ancestry_field = 'parent_url';
  public $children_cache = 'children';

};