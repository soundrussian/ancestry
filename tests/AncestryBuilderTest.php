<?php namespace Soundrussian\Ancestry\Tests;

require_once 'DatabaseTestCase.php';

use \Ancestry;

class AncestryBuilderTest extends DatabaseTestCase {

  public function testNewModelIsRoot() {
    $dummy = new Dummy;
    $this->assertTrue(Ancestry::query($dummy)->isRoot());
  }

  public function testMakesModelAChildOf() {
    $child  = new Dummy;
    $child->url = 'test_url';
    $child->save();
    $parent = $this->randomDummy();
    Ancestry::make($child)->childOf($parent);
    $this->assertEquals(Ancestry::query($parent)->pathIds(), Ancestry::query($child)->ancestorIds());
  }

  public function testMakesModelTheParent() {
    $child  = new Dummy;
    $child->url = 'test_url';
    $child->save();
    $parent = $this->randomDummy();
    Ancestry::make($parent)->parentOf($child);
    $this->assertEquals(Ancestry::query($parent)->pathIds(), Ancestry::query($child)->ancestorIds());
  }

  public function testPersistingParent() {
    $child  = new Dummy;
    $parent = new Dummy;
    Ancestry::make($parent)->parentOf($child);
    $this->assertEquals($parent->self_url, Ancestry::query($child)->parentId());
  }

  public function testMovingSubtree() {
    $old_root = Dummy::find(1);
    $new_root = Dummy::find(2);
    $child    = Dummy::find(6);
    $parent   = Dummy::find(9);
    Ancestry::make($child)->childOf($parent);
    $subtree  = Ancestry::query($child)->subtree()->get();
    foreach($subtree as $leaf) {
      $this->assertEquals('2', Ancestry::query($leaf)->rootId());
      $this->assertNotContains($leaf->id, Ancestry::query($old_root)->descendantIds());
      $this->assertContains($leaf->id, Ancestry::query($new_root)->descendantIds());
    }
  }

  public function testMakeRoot() {
    $model = Dummy::find(5);
    $id = Ancestry::query($model)->parent()->id;

    Ancestry::make($model)->root();

    $child = Dummy::find(7);

    $this->assertTrue(Ancestry::query($model)->isRoot());
    $this->assertNotContains(1, Ancestry::query($child)->ancestorIds());
    $this->assertEquals([5, 6], Ancestry::query($child)->ancestorIds());
    $this->assertEquals(2, Dummy::find($id)->children);
  }

  public function testUpdatedChildrenCache() {
    $child  = new Dummy;
    $parent = $this->randomDummy();
    $cache_before = $parent->children;
    Ancestry::make($child)->childOf($parent);
    $this->assertEquals($cache_before + 1, $parent->children);
  }

  public function testUpdatedChildrenCacheOfPreviousParent() {
    $child  = Dummy::find(9);
    $prev_parent = Dummy::find(2);
    $new_parent  = Dummy::find(1);
    $prev_cache_before = $prev_parent->children;
    Ancestry::make($child)->childOf($new_parent);
    $prev_parent = Dummy::find(2);
    $this->assertEquals($prev_cache_before - 1, $prev_parent->children);
  }

  protected function randomDummy() {
    return Dummy::find(mt_rand(1, 10));
  }
}