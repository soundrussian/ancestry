<?php

use Illuminate\Database\Migrations\Migration;

class CreateDummyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dummies', function ($table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable()->index();
            $table->string('url')->nullable()->index();
            $table->string('parent_url')->nullable()->index();
            $table->integer('children')->index()->default(0);
        });

        // Test data:
        // 1
        // |- 4
        // |- 5
        // |  |- 6
        // |     |- 7
        // |- 8
        //    |-10
        // 2
        // |- 9
        //
        // 3

        $data = [
            ['id' => 1,  'parent_id' => null, 'url' => '1',  'children' => 3, 'parent_url' => null],
            ['id' => 2,  'parent_id' => null, 'url' => '2',  'children' => 1, 'parent_url' => null],
            ['id' => 3,  'parent_id' => null, 'url' => '3',  'children' => 0, 'parent_url' => null],
            ['id' => 4,  'parent_id' => 1,    'url' => '4',  'children' => 0, 'parent_url' => '1'],
            ['id' => 5,  'parent_id' => 1,    'url' => '5',  'children' => 1, 'parent_url' => '1'],
            ['id' => 6,  'parent_id' => 5,    'url' => '6',  'children' => 1, 'parent_url' => '1/5'],
            ['id' => 7,  'parent_id' => 6,    'url' => '7',  'children' => 0, 'parent_url' => '1/5/6'],
            ['id' => 8,  'parent_id' => 1,    'url' => '8',  'children' => 1, 'parent_url' => '1'],
            ['id' => 9,  'parent_id' => 2,    'url' => '9',  'children' => 0, 'parent_url' => '2'],
            ['id' => 10, 'parent_id' => 8,    'url' => '10', 'children' => 0, 'parent_url' => '1/8'],
        ];

        DB::table('dummies')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dummies');
    }
}