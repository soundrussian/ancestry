<?php namespace Soundrussian\Ancestry\Tests;

use \Ancestry;

require_once 'DatabaseTestCase.php';

class AncestryObserverTest extends DatabaseTestCase {

  public function testDestroyStrategy() {
    Dummy::observe(Ancestry::destroyOrphans());

    $root = Dummy::find(1);
    $descendants = Ancestry::query($root)->descendantIds();

    $root->delete();

    foreach($descendants as $id) {
      $this->assertNull(Dummy::find($id));
    }
  }

  public function testAdoptStrategy() {
    Dummy::observe(Ancestry::adoptOrphans());

    $root = Dummy::find(5);
    $descendants = Ancestry::query($root)->descendantIds();

    $root->delete();

    $child = Dummy::find(6);
    $this->assertEquals(1, Ancestry::query($child)->parentId());
  }

  public function testAdoptStrategyRootifiesIfNoParentsLeft() {
    Dummy::observe(Ancestry::adoptOrphans());

    $root = Dummy::find(2);
    $root->delete();

    $child = Dummy::find(9);
    $this->assertEquals(true, Ancestry::query($child)->isRoot());
  }

  public function testRootifyStrategy() {
    Dummy::observe(Ancestry::rootifyOrphans());

    $root = Dummy::find(1);
    $children = Ancestry::query($root)->childrenIds();

    $root->delete();

    foreach($children as $child) {
      $model = Dummy::find($child);
      $this->assertTrue(Ancestry::query($model)->isRoot());
    }
  }

  /**
   * @expectedException Soundrussian\Ancestry\Observers\DeletingModelWithChildrenException
   */
  public function testExceptionStrategy() {
    Dummy::observe(Ancestry::throwExceptionIfOrphans());

    $root = Dummy::find(1);
    $root->delete();
  }

}