<?php namespace Soundrussian\Ancestry\Tests;

abstract class DatabaseTestCase extends \Orchestra\Testbench\TestCase {
  public function setUp() {
    parent::setUp();

    require_once 'Dummy.php';

    $artisan = $this->app->make('artisan');

    $artisan->call('migrate', array(
      '--database' => 'testbench',
      '--path'     => '../tests/migrations',
    ));
  }

  protected function getEnvironmentSetup($app) {
    $app['path.base'] = __DIR__ . '/../src';

    $app['config']->set('database.default', 'testbench');
    $app['config']->set('database.connections.testbench', array(
      'driver'   => 'sqlite',
      'database' => ':memory:',
      'prefix'   => '',
    ));
  }

  protected function getPackageProviders() {
    return array(
      'Soundrussian\Ancestry\AncestryServiceProvider',
    );
  }

  protected function getPackageAliases() {
    return array(
      'Ancestry' => 'Soundrussian\Ancestry\Facades\Ancestry'
    );
  }

  protected function _debug($value, $msg='') {
    echo $msg."\n";
    echo "===============\n";
    print_r($value);
    echo "\n===============\n";
    ob_flush();
    exit('Stopped to debug');
  }
}