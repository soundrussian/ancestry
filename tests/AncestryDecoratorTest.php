<?php namespace Soundrussian\Ancestry\Tests;

require_once 'DatabaseTestCase.php';

use \Ancestry;

/**
 * AncestryDecorator is a class to decorate a model to provide different ancestry-related queries:
 * parent           Returns the parent of the record, nil for a root node
 * parent_id        Returns the id of the parent of the record, nil for a root node
 * root             Returns the root of the tree the record is in, self for a root node
 * root_id          Returns the id of the root of the tree the record is in
 * root?, is_root?  Returns true if the record is a root node, false otherwise
 * ancestor_ids     Returns a list of ancestor ids, starting with the root id and ending with the parent id
 * ancestors        Scopes the model on ancestors of the record
 * path_ids         Returns a list the path ids, starting with the root id and ending with the node's own id
 * path             Scopes model on path records of the record
 * children         Scopes the model on children of the record
 * child_ids        Returns a list of child ids
 * has_children?    Returns true if the record has any children, false otherwise
 * is_childless?    Returns true is the record has no children, false otherwise
 * siblings         Scopes the model on siblings of the record, the record itself is included
 * sibling_ids      Returns a list of sibling ids
 * has_siblings?    Returns true if the record's parent has more than one child
 * is_only_child?   Returns true if the record is the only child of its parent
 * descendants      Scopes the model on direct and indirect children of the record
 * descendant_ids   Returns a list of a descendant ids
 * subtree          Scopes the model on descendants and itself
 * subtree_ids      Returns a list of all ids in the record's subtree
 * depth            Return the depth of the node, root nodes are at depth 0
 */

class AncestryDecoratorTest extends DatabaseTestCase {

  public function testParent() {

    $examples = [
      [ 'id' => 1,  'exists' => false, 'parent_id' => null],
      [ 'id' => 2,  'exists' => false, 'parent_id' => null],
      [ 'id' => 3,  'exists' => false, 'parent_id' => null],
      [ 'id' => 4,  'exists' => true,  'parent_id' => 1],
      [ 'id' => 5,  'exists' => true,  'parent_id' => 1],
      [ 'id' => 8,  'exists' => true,  'parent_id' => 1],
      [ 'id' => 7,  'exists' => true,  'parent_id' => 6],
      [ 'id' => 6,  'exists' => true,  'parent_id' => 5],
      [ 'id' => 10, 'exists' => true,  'parent_id' => 8],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);
      $parent = Ancestry::query($dummy)->parent();

      if ($ex['exists']) {
        $this->assertInstanceOf('Soundrussian\Ancestry\Tests\Dummy', $parent);
      } else {
        $this->assertNull($parent);
      }

      if (!empty($ex['parent_id'])) {
        $this->assertEquals($ex['parent_id'], $parent->url);
      }
    };
  }

  public function testParentId() {
    $examples = [
      [ 'id' => 1,  'parent' => null],
      [ 'id' => 2,  'parent' => null],
      [ 'id' => 3,  'parent' => null],
      [ 'id' => 4,  'parent' => 1],
      [ 'id' => 5,  'parent' => 1],
      [ 'id' => 6,  'parent' => 5],
      [ 'id' => 7,  'parent' => 6],
      [ 'id' => 8,  'parent' => 1],
      [ 'id' => 9,  'parent' => 2],
      [ 'id' => 10, 'parent' => 8],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['parent'], \Ancestry::query($dummy)->parentId());
    }
  }

  public function testRootId() {
    $examples = [
      ['id' => 1,  'root' => 1],
      ['id' => 2,  'root' => 2],
      ['id' => 3,  'root' => 3],
      ['id' => 4,  'root' => 1],
      ['id' => 5,  'root' => 1],
      ['id' => 6,  'root' => 1],
      ['id' => 7,  'root' => 1],
      ['id' => 8,  'root' => 1],
      ['id' => 9,  'root' => 2],
      ['id' => 10, 'root' => 1],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['root'], Ancestry::query($dummy)->rootId());
    }
  }

  public function testRoot() {
    $examples = [
      ['id' => 1,  'root' => 1],
      ['id' => 2,  'root' => 2],
      ['id' => 3,  'root' => 3],
      ['id' => 4,  'root' => 1],
      ['id' => 5,  'root' => 1],
      ['id' => 6,  'root' => 1],
      ['id' => 7,  'root' => 1],
      ['id' => 8,  'root' => 1],
      ['id' => 9,  'root' => 2],
      ['id' => 10, 'root' => 1],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['root'], Ancestry::query($dummy)->root()->id);
    }
  }

  public function testIsRoot() {
    $examples = [
      ['id' => 1,  'root' => true],
      ['id' => 2,  'root' => true],
      ['id' => 3,  'root' => true],
      ['id' => 4,  'root' => false],
      ['id' => 5,  'root' => false],
      ['id' => 6,  'root' => false],
      ['id' => 7,  'root' => false],
      ['id' => 8,  'root' => false],
      ['id' => 9,  'root' => false],
      ['id' => 10, 'root' => false],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['root'], Ancestry::query($dummy)->isRoot());
    }
  }

  public function testAncestorsIds() {
    $examples = [
      ['id' => 1,  'expected' => array()],
      ['id' => 2,  'expected' => array()],
      ['id' => 3,  'expected' => array()],
      ['id' => 4,  'expected' => array(1)],
      ['id' => 5,  'expected' => array(1)],
      ['id' => 6,  'expected' => array(1, 5)],
      ['id' => 7,  'expected' => array(1, 5, 6)],
      ['id' => 8,  'expected' => array(1)],
      ['id' => 9,  'expected' => array(2)],
      ['id' => 10, 'expected' => array(1, 8)],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->ancestorIds());
    }
  }

  public function testAncestors() {
    $examples = [
      ['id' => 1,  'expected' => array()],
      ['id' => 2,  'expected' => array()],
      ['id' => 3,  'expected' => array()],
      ['id' => 4,  'expected' => array(1)],
      ['id' => 5,  'expected' => array(1)],
      ['id' => 6,  'expected' => array(1, 5)],
      ['id' => 7,  'expected' => array(1, 5, 6)],
      ['id' => 8,  'expected' => array(1)],
      ['id' => 9,  'expected' => array(2)],
      ['id' => 10, 'expected' => array(1, 8)],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->ancestors()->lists('id'));
    }
  }

  public function testPathIds() {
    $examples = [
      ['id' => 1,  'expected' => array(1)],
      ['id' => 2,  'expected' => array(2)],
      ['id' => 3,  'expected' => array(3)],
      ['id' => 4,  'expected' => array(1, 4)],
      ['id' => 5,  'expected' => array(1, 5)],
      ['id' => 6,  'expected' => array(1, 5, 6)],
      ['id' => 7,  'expected' => array(1, 5, 6, 7)],
      ['id' => 8,  'expected' => array(1, 8)],
      ['id' => 9,  'expected' => array(2, 9)],
      ['id' => 10, 'expected' => array(1, 8, 10)],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->pathIds());
    }
  }

  public function testPath() {
    $examples = [
      ['id' => 1,  'expected' => array(1)],
      ['id' => 2,  'expected' => array(2)],
      ['id' => 3,  'expected' => array(3)],
      ['id' => 4,  'expected' => array(1, 4)],
      ['id' => 5,  'expected' => array(1, 5)],
      ['id' => 6,  'expected' => array(1, 5, 6)],
      ['id' => 7,  'expected' => array(1, 5, 6, 7)],
      ['id' => 8,  'expected' => array(1, 8)],
      ['id' => 9,  'expected' => array(2, 9)],
      ['id' => 10, 'expected' => array(1, 8, 10)],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->path()->lists('id'));
    }
  }

  public function testChildren() {
    $examples = [
      ['id' => 1,  'expected' => array(4, 5, 8)],
      ['id' => 2,  'expected' => array(9)],
      ['id' => 3,  'expected' => array()],
      ['id' => 4,  'expected' => array()],
      ['id' => 5,  'expected' => array(6)],
      ['id' => 6,  'expected' => array(7)],
      ['id' => 7,  'expected' => array()],
      ['id' => 8,  'expected' => array(10)],
      ['id' => 9,  'expected' => array()],
      ['id' => 10, 'expected' => array()],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->children()->lists('id'));
    }
  }

  public function testChildrenIds() {
    $examples = [
      ['id' => 1,  'expected' => array(4, 5, 8)],
      ['id' => 2,  'expected' => array(9)],
      ['id' => 3,  'expected' => array()],
      ['id' => 4,  'expected' => array()],
      ['id' => 5,  'expected' => array(6)],
      ['id' => 6,  'expected' => array(7)],
      ['id' => 7,  'expected' => array()],
      ['id' => 8,  'expected' => array(10)],
      ['id' => 9,  'expected' => array()],
      ['id' => 10, 'expected' => array()],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->childrenIds());
    }
  }

  public function testHasChildren() {
    $examples = [
      ['id' => 1,  'expected' => true],
      ['id' => 2,  'expected' => true],
      ['id' => 3,  'expected' => false],
      ['id' => 4,  'expected' => false],
      ['id' => 5,  'expected' => true],
      ['id' => 6,  'expected' => true],
      ['id' => 7,  'expected' => false],
      ['id' => 8,  'expected' => true],
      ['id' => 9,  'expected' => false],
      ['id' => 10, 'expected' => false],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->hasChildren());
    }
  }

  public function testIsChildless() {
    $examples = [
      ['id' => 1,  'expected' => false],
      ['id' => 2,  'expected' => false],
      ['id' => 3,  'expected' => true],
      ['id' => 4,  'expected' => true],
      ['id' => 5,  'expected' => false],
      ['id' => 6,  'expected' => false],
      ['id' => 7,  'expected' => true],
      ['id' => 8,  'expected' => false],
      ['id' => 9,  'expected' => true],
      ['id' => 10, 'expected' => true],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->isChildless());
    }
  }

  public function testSiblings() {
    $examples = [
      ['id' => 1,  'expected' => array(1, 2, 3)],
      ['id' => 2,  'expected' => array(1, 2, 3)],
      ['id' => 3,  'expected' => array(1, 2, 3)],
      ['id' => 4,  'expected' => array(4, 5, 8)],
      ['id' => 5,  'expected' => array(4, 5, 8)],
      ['id' => 6,  'expected' => array(6)],
      ['id' => 7,  'expected' => array(7)],
      ['id' => 8,  'expected' => array(4, 5, 8)],
      ['id' => 9,  'expected' => array(9)],
      ['id' => 10, 'expected' => array(10)],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->siblings()->lists('id'));
    }
  }

  public function testSiblingIds() {
    $examples = [
      ['id' => 1,  'expected' => array(1, 2, 3)],
      ['id' => 2,  'expected' => array(1, 2, 3)],
      ['id' => 3,  'expected' => array(1, 2, 3)],
      ['id' => 4,  'expected' => array(4, 5, 8)],
      ['id' => 5,  'expected' => array(4, 5, 8)],
      ['id' => 6,  'expected' => array(6)],
      ['id' => 7,  'expected' => array(7)],
      ['id' => 8,  'expected' => array(4, 5, 8)],
      ['id' => 9,  'expected' => array(9)],
      ['id' => 10, 'expected' => array(10)],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->siblingIds());
    }
  }

  public function testHasSiblings() {
    $examples = [
      ['id' => 1,  'expected' => true],
      ['id' => 2,  'expected' => true],
      ['id' => 3,  'expected' => true],
      ['id' => 4,  'expected' => true],
      ['id' => 5,  'expected' => true],
      ['id' => 6,  'expected' => false],
      ['id' => 7,  'expected' => false],
      ['id' => 8,  'expected' => true],
      ['id' => 9,  'expected' => false],
      ['id' => 10, 'expected' => false],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->hasSiblings());
    }
  }

  public function testIsOnlyChild() {
    $examples = [
      ['id' => 1,  'expected' => false],
      ['id' => 2,  'expected' => false],
      ['id' => 3,  'expected' => false],
      ['id' => 4,  'expected' => false],
      ['id' => 5,  'expected' => false],
      ['id' => 6,  'expected' => true],
      ['id' => 7,  'expected' => true],
      ['id' => 8,  'expected' => false],
      ['id' => 9,  'expected' => true],
      ['id' => 10, 'expected' => true],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->isOnlyChild());
    }
  }

  public function testDescendants() {
    $examples = [
      ['id' => 1,  'expected' => array('4', '5', '6', '7', '8', '10')],
      ['id' => 2,  'expected' => array('9')],
      ['id' => 3,  'expected' => array()],
      ['id' => 4,  'expected' => array()],
      ['id' => 5,  'expected' => array('6', '7')],
      ['id' => 6,  'expected' => array('7')],
      ['id' => 7,  'expected' => array()],
      ['id' => 8,  'expected' => array('10')],
      ['id' => 9,  'expected' => array()],
      ['id' => 10, 'expected' => array()],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->descendants()->lists('id'));
    }
  }

  public function testDescendantIds() {
    $examples = [
      ['id' => 1,  'expected' => array('4', '5', '6', '7', '8', '10')],
      ['id' => 2,  'expected' => array('9')],
      ['id' => 3,  'expected' => array()],
      ['id' => 4,  'expected' => array()],
      ['id' => 5,  'expected' => array('6', '7')],
      ['id' => 6,  'expected' => array('7')],
      ['id' => 7,  'expected' => array()],
      ['id' => 8,  'expected' => array('10')],
      ['id' => 9,  'expected' => array()],
      ['id' => 10, 'expected' => array()],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->descendantIds());
    }
  }

  public function testSubtree() {
    $examples = [
      ['id' => 1,  'expected' => array('1', '4', '5', '6', '7', '8', '10')],
      ['id' => 2,  'expected' => array('2', '9')],
      ['id' => 3,  'expected' => array('3')],
      ['id' => 4,  'expected' => array('4')],
      ['id' => 5,  'expected' => array('5', '6', '7')],
      ['id' => 6,  'expected' => array('6', '7')],
      ['id' => 7,  'expected' => array('7')],
      ['id' => 8,  'expected' => array('8', '10')],
      ['id' => 9,  'expected' => array('9')],
      ['id' => 10, 'expected' => array('10')],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->subtree()->lists('id'));
    }
  }

  public function testSubtreeIds() {
    $examples = [
      ['id' => 1,  'expected' => array('1', '4', '5', '6', '7', '8', '10')],
      ['id' => 2,  'expected' => array('2', '9')],
      ['id' => 3,  'expected' => array('3')],
      ['id' => 4,  'expected' => array('4')],
      ['id' => 5,  'expected' => array('5', '6', '7')],
      ['id' => 6,  'expected' => array('6', '7')],
      ['id' => 7,  'expected' => array('7')],
      ['id' => 8,  'expected' => array('8', '10')],
      ['id' => 9,  'expected' => array('9')],
      ['id' => 10, 'expected' => array('10')],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->subtreeIds());
    }
  }

  public function testDepth() {
    $examples = [
      ['id' => 1,  'expected' => 0],
      ['id' => 2,  'expected' => 0],
      ['id' => 3,  'expected' => 0],
      ['id' => 4,  'expected' => 1],
      ['id' => 5,  'expected' => 1],
      ['id' => 6,  'expected' => 2],
      ['id' => 7,  'expected' => 3],
      ['id' => 8,  'expected' => 1],
      ['id' => 9,  'expected' => 1],
      ['id' => 10, 'expected' => 2],
    ];

    foreach($examples as $ex) {
      $dummy = Dummy::find($ex['id']);

      $this->assertEquals($ex['expected'], Ancestry::query($dummy)->depth());
    }
  }

}