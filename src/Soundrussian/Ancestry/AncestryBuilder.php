<?php namespace Soundrussian\Ancestry;
/**
 * Ancestry Builder
 *
 * @author  Pavel Nosov
 * @license http://opensource.org/licenses/MIT MIT
 */

/**
 * Ancestry builder for Eloquent models.
 *
 * <b>Warning!</b> The methods of this class save both child and parent models, so
 * watch out side effects.<br>
 *
 * This class establishes ancestry relation between two models, making one the parent
 * of another. It also makes sure that the subtree ancestry stays consistent, thus
 * allowing moving the whole subtrees.
 *
 * To set the fields used for ancestry in model, set some properties:
 * <pre>
 * public $ancestry_id    = 'url';
 * public $ancestry_field = 'parent_url';
 * </pre>
 *
 * By default, <code>$ancestry_id</code> is <code>'id'</code>, and <code>$ancestry_field</code>
 * is <code>'ancestry'</code>.
 *
 * This class is exposed via <code>Ancestry::make()</code> to make DSL-like calls possible:
 *
 * <pre>
 * Ancestry::make($parent)->parentOf($child);
 * Ancestry::make($child)->childOf($parent);
 * </pre>
 *
 * So there's no need to remember the order of arguments. Ain't it cool?
 *
 * @see    Soundrussian\Ancestry\Ancestry::make()
 * @author Pavel Nosov
 */
class AncestryBuilder {

  /**
   * Constructor, accepting a reference to Eloquent model.
   *
   * @param  mixed $model
   * @return AncestryBuilder
   */
  public function __construct(&$model) {
    $this->_model = $model;
  }

  /**
   * Makes the argument model the parent of the model passed to constructior.
   * <b>
   *
   * @param mixed $parent
   */
  public function childOf($parent) {
    $this->_setAncestry($this->_model, $parent);
  }

  /**
   * Makes the argument model a child of the model passed to constructior.
   *
   * @param mixed $child
   */
  public function parentOf($child) {
    $this->_setAncestry($child, $this->_model);
  }

  /**
   * Moves the model to root
   */
  public function root() {
    $decorator  = new AncestryDecorator($this->_model);
    $parent     = $decorator->parent();
    $ancestry   = $this->_ancestry_field();
    $ids        = $decorator->subtreeIds();
    $old_path   = $this->_model->{$ancestry};
    $new_path   = '';
    $this->_updateDescendants($this->_model->getTable(), $ids, $old_path, $new_path);
    $this->_model->{$ancestry} = null;
    $this->_updateChildrenCount($parent);
    $this->_model->save();
  }

  /**
   * Returns ancestry field
   *
   * @return string
   */
  private function _ancestry_field()
  {
    if (isset($this->_model->ancestry_field))
    {
      return $this->_model->ancestry_field;
    } else {
      return 'ancestry';
    }
  }

  /**
   * Returns ancestry id
   *
   * @return string
   */
  private function _ancestry_id()
  {
    if (isset($this->_model->ancestry_id))
    {
      return $this->_model->ancestry_id;
    } else {
      return 'id';
    }
  }

  /**
   * Method performing the magic. It updates child's subtree to set the
   * new parent.
   *
   * @param mixed $child
   * @param mixed $parent
   */
  private function _setAncestry($child, $parent) {
    $decorator  = new AncestryDecorator($child);
    $parent_decorator = new AncestryDecorator($parent);
    $current_parent = $decorator->parent();
    $ancestry   = $this->_ancestry_field();
    $id         = $this->_ancestry_id();
    $suffix     = empty($parent->{$ancestry}) ? $parent->{$id} : '/'.$parent->{$id};
    $old_path   = $child->{$ancestry};
    $ids        = $decorator->descendantIds();
    $new_path   = $parent->{$ancestry} . $suffix;
    $this->_updateDescendants($child->getTable(), $ids, $old_path, $new_path);
    $child->{$ancestry} = $new_path;
    $child->save();
    $this->_updateChildrenCount($parent);
    $this->_updateChildrenCount($current_parent);
  }

  /**
   * Updates child's descendant ancestry to keep the hierarchy consistent.
   *
   * @param string $table Table name to perform the operation on
   * @param array  $ids   Ids of the models to be changed.
   * @param string $old_path Part of the ancestry to be rewritten
   * @param string $new_path New ancestry prefix
   */
  private function _updateDescendants($table, $ids, $old_path, $new_path) {
    if (empty($ids)) return;
    $ids   = implode("', '", $ids);
    $id    = $this->_ancestry_id();
    $ancestry = $this->_ancestry_field();
    $query = "UPDATE $table SET $ancestry = REPLACE($ancestry, '$old_path', '$new_path') WHERE $id IN ('$ids')";
    \DB::statement($query);
  }

  /**
   * Updates children count on parent model
   *
   * @param Eloquent $model
   * @param string $ancestry
   */
  private function _updateChildrenCount($parent)
  {
    if (empty($parent)) return;
    $cache_field    = $parent->children_cache;
    if (isset($cache_field))
    {
      $ancestry = new Ancestry;
      $parent->{$cache_field} = count($ancestry->query($parent)->childrenIds());
    }
    $parent->save();
  }

  /**
   * Keeps a reference to model
   */
  private $_model = null;

}