<?php namespace Soundrussian\Ancestry;

class Ancestry {

  public static function lol() {
    return 'lol';
  }

  public function query($model) {
    return new AncestryDecorator($model);
  }

  public function make($model) {
    return new AncestryBuilder($model);
  }

  public function destroyOrphans() {
    return new Observers\DestroyOrphans;
  }

  public function adoptOrphans() {
    return new Observers\AdoptOrphans;
  }

  public function rootifyOrphans() {
    return new Observers\RootifyOrphans;
  }

  public function throwExceptionIfOrphans() {
    return new Observers\ExceptionOrphans;
  }

}