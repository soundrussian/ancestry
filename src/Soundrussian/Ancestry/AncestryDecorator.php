<?php
/**
 * Ancestry Decorator
 *
 * @author  Pavel Nosov
 * @license http://opensource.org/licenses/MIT MIT
 */
namespace Soundrussian\Ancestry;

/**
 * Ancestry decorator for Eloquent models.
 *
 * This class is used behind the scenes by Ancestry to provide different methods to scope for
 * hierarchy. It acts as a decorator, accepting a model and empowering it with hierarchy-conserned
 * method. It is exposed by <i>Ancestry::query()</i> method.
 *
 * To set the fields used for ancestry in model, set some properties:
 * <pre>
 * public $ancestry_id    = 'url';
 * public $ancestry_field = 'parent_url';
 * </pre>
 *
 * By default, <code>$ancestry_id</code> is <code>'id'</code>, and <code>$ancestry_field</code>
 * is <code>'ancestry'</code>.
 *
 * It returns queries where possible, so that it could be chained, for example:
 *
 * <pre>
 * Ancestry::query($employee)->children()->where('prefersCoffee', '=', true);
 * </pre>
 *
 * Since it returns queries, you need to call <i>get()</i> to get the actual results:
 *
 * <pre>
 * Ancestry::query($employee)->subtree()->get();
 * </pre>
 *
 * @see    Soundrussian\Ancestry\Ancestry::query()
 * @author Pavel Nosov
 */
class AncestryDecorator {

  /**
   * Constructor, accepting a reference to Eloquent model.
   *
   * @param  mixed $model
   * @return AncestryDecorator
   */
  public function __construct(&$model) {
    $this->_model = $model;
  }

  /**
   * Returns the parent of the given element, or _null_ if the given model is root.
   *
   * @return mixed
   */
  public function parent() {
    $parent_id = $this->parentId();

    // If this is a root model, return query that always returns an ampty set
    $result = $this->_find($parent_id);
    return $result;
  }

  /**
   * Returns the root for the given model, or the model itself if it is root.
   *
   * @return mixed
   */
  public function root() {
    $root_id = $this->rootId();
    return ($root_id == $this->_id()) ? $this->_model : $this->_find($root_id);
  }

  /**
   * Returns query for ancestors of the given model, which does not include the model itself.
   *
   * @return mixed
   */
  public function ancestors() {
    $ids = $this->ancestorIds();
    if (empty($ids)) return $this->_model->where(\Db::raw('1 = 2'));
    return $this->_model->whereIn($this->_ancestry_id(), $ids);
  }

  /**
   * Returns query for ancestors of the given model and the model itself.
   *
   * @return mixed
   */
  public function path() {
    return $this->_model->whereIn('id', $this->pathIds());
  }

  /**
   * Returns query for children (immediate descendants) of the given model.
   *
   * @return mixed
   */
  public function children() {
    $id = $this->_id();
    $ancestry = $this->_ancestry_field();
    return $this->_model
              ->where($ancestry, '=', $id)
              ->orWhere($ancestry, 'LIKE', '%/'.$id);
  }

  /**
   * Returns siblings of the given model.
   *
   * @return mixed
   */
  public function siblings() {
    $ancestry = $this->_ancestry_field();
    return $this->_model->where($ancestry, '=', $this->_ancestry());
  }

  /**
   * Returns query for descendants of the given model.
   *
   * @return mixed
   */
  public function descendants() {
    $ans = $this->_ancestry();
    $id  = $this->_id();
    $ancestry = $this->_ancestry_field();
    if (empty($ans)) {
      return $this->_model->where($ancestry, 'LIKE', "$id%");
    } else {
      return $this->_model->where($ancestry, 'LIKE', "$ans/$id%");
    }
  }

  /**
   * Returns query for subtree of the given model, which includes the model itself and its descendants.
   *
   * @return mixed
   */
  public function subtree() {
    return $this->descendants()->withTrashed()->orWhere('id', '=', $this->_id());
  }

  /**
   * Returns id of the parent of the given model.
   *
   * @return string
   */
  public function parentId() {
    $tree      = $this->_ancestry_arr();
    $parent_id = end($tree);
    return $parent_id;
  }

  /**
   * Returns id of the root of the given model, which is model id if it has no parent.
   *
   * @return string
   */
  public function rootId() {
    $root_id = $this->_ancestry_arr()[0];
    return (empty($root_id)) ? $this->_id() : $root_id;
  }

  /**
   * Returns array of ids of the model's ancestors.
   *
   * @return array
   */
  public function ancestorIds() {
    $ids = $this->_ancestry_arr();
    return empty($ids[0]) ? array() : $ids;
  }

  /**
   * Returns arrays of ids of the model's ancestors and the model id.
   *
   * @return array
   */
  public function pathIds() {
    $path_ids = $this->ancestorIds();
    array_push($path_ids, $this->_id());
    return $path_ids;
  }

  /**
   * Returns array of ids of the model's children.
   *
   * @return array
   */
  public function childrenIds() {
    return $this->children()->lists('id');
  }

  /**
   * Returns array of ids of the model's siblings.
   *
   * @return array
   */
  public function siblingIds() {
    return $this->siblings()->lists('id');
  }

  /**
   * Returns array of ids of the model's descendants.
   *
   * @return @array
   */
  public function descendantIds() {
    return $this->descendants()->lists('id');
  }

  /**
   * Returns array of ids of the model's descendants and the model id.
   *
   * @return array
   */
  public function subtreeIds() {
    return $this->subtree()->lists('id');
  }

  /**
   * Tells if the model is root.
   *
   * @return bool
   */
  public function isRoot() {
    $ancestry = $this->_ancestry();
    return $ancestry === null;
  }

  /**
   * Tells if the model has children.
   *
   * @return bool
   */
  public function hasChildren() {
    $children_cache = $this->_model->children_cache;
    if (isset($children_cache))
    {
      return $this->_model->{$children_cache} > 0;
    } else {
      return $this->children()->exists();
    }
  }

  /**
   * Tells if the model has no children.
   *
   * @return bool
   */
  public function isChildless() {
    return !$this->hasChildren();
  }

  /**
   * Tells if the model has siblings.
   *
   * @return bool
   */
  public function hasSiblings() {
    $ansectry = $this->_ancestry_field();
    $id = $this->_ancestry_id();
    return $this->_model->where($ansectry, '=', $this->_ancestry())
            ->where($id, '!=', $this->_id())->exists();
  }

  /**
   * Tells if the model is the only child of its parent.
   *
   * @return bool
   */
  public function isOnlyChild() {
    return !$this->hasSiblings();
  }

  /**
   * Returns ancestry depth, which is 0 for root model.
   *
   * @return int
   */
  public function depth() {
    if (empty($this->_ancestry_arr()[0])) {
      return 0;
    } else {
      return count($this->_ancestry_arr());
    }
  }

  /**
   * Delegates call to <i>find</id> to Eloquent model.
   *
   * @param  mixed $id
   * @return mixed
   */
  private function _find($id) {
    $ancestry_id = $this->_ancestry_id();
    return $this->_model->where($ancestry_id, '=', $id)->withTrashed()->first();
  }

  /**
   * Gets id of the model.
   *
   * @return mixed
   */
  private function _id() {
    $ancestry_id = $this->_ancestry_id();
    return $this->_model->{$ancestry_id};
  }

  /**
   * Returns ancestry field
   *
   * @return string
   */
  private function _ancestry_field()
  {
    if (isset($this->_model->ancestry_field))
    {
      return $this->_model->ancestry_field;
    } else {
      return 'ancestry';
    }
  }

  /**
   * Returns ancestry id
   *
   * @return string
   */
  private function _ancestry_id()
  {
    if (isset($this->_model->ancestry_id))
    {
      return $this->_model->ancestry_id;
    } else {
      return 'id';
    }
  }

  /**
   * Returns ancestry string of the model.
   *
   * @return string
   */
  private function _ancestry() {
    $ancestry = $this->_ancestry_field();
    return $this->_model->{$ancestry};
  }

  /**
   * Returns the array of ids from the ancestry string.
   *
   * @return array
   */
  private function _ancestry_arr() {
    $ancestry = preg_replace('/^(\/+)/', '', $this->_ancestry());
    return explode('/', $ancestry);
  }

  /**
   * Reference to the model we'll be querying for.
   */
  private $_model = null;

}