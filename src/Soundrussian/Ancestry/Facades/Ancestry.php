<?php namespace Soundrussian\Ancestry\Facades;

use Illuminate\Support\Facades\Facade;

class Ancestry extends Facade {

  protected static function getFacadeAccessor() { return 'ancestry'; }

}