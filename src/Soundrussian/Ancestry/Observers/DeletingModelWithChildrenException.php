<?php namespace Soundrussian\Ancestry\Observers;

use Soundrussian\Ancestry\Ancestry as Ancestry;
use Soundrussian\Ancestry\AncestryDecorator;

class DeletingModelWithChildrenException extends \Exception {

  public function __construct($model)
  {
  	$decorator   = new AncestryDecorator($model);
    $children    = implode(', ', $decorator->childrenIds());
    $model_class = get_class($model);
    $message     = "Attempting to destroy $model_class with id = $model->id, ";
    $message    .= "which has the following children: [$children]. ";
    $message    .= "This behavior is set by model observer.";

    parent::__construct($message);
  }

}