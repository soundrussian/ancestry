<?php namespace Soundrussian\Ancestry\Observers;

use Soundrussian\Ancestry\Ancestry as Ancestry;
use Soundrussian\Ancestry\AncestryDecorator;

class DestroyOrphans {

  public function deleting($model)
  {
  	$decorator = new AncestryDecorator($model);
    $decorator->descendants()->delete();
  }
}