<?php namespace Soundrussian\Ancestry\Observers;

use Soundrussian\Ancestry\Ancestry as Ancestry;
use Soundrussian\Ancestry\AncestryDecorator;
use Soundrussian\Ancestry\AncestryBuilder;

class AdoptOrphans {

  public function deleting($model)
  {
    $decorator = new AncestryDecorator($model);
    $parent    = $decorator->parent();
    $children  = $decorator->children()->get();

    if ($parent) {
      $this->setChildrenParents($children, $parent);
    } else {
      $this->makeChildrenRoot($children);
    }
  }

  private function setChildrenParents($children, $parent)
  {
    $builder = new AncestryBuilder($parent);
    foreach($children as $child) {
        $builder->parentOf($child);
    }
  }

  private function makeChildrenRoot($children)
  {
    foreach($children as $child) {
      $builder = new AncestryBuilder($child);
      $builder->root();
    }
  }
}