<?php namespace Soundrussian\Ancestry\Observers;

use Soundrussian\Ancestry\Ancestry as Ancestry;
use Soundrussian\Ancestry\AncestryDecorator;

class ExceptionOrphans {

  public function deleting($model)
  {
  	$decorator = new AncestryDecorator($model);
    if ($decorator->hasChildren())
    {
      throw new DeletingModelWithChildrenException($model);
    }
  }
}