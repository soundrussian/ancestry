<?php namespace Soundrussian\Ancestry\Observers;

use Soundrussian\Ancestry\Ancestry as Ancestry;
use Soundrussian\Ancestry\AncestryDecorator;
use Soundrussian\Ancestry\AncestryBuilder;

class RootifyOrphans {

  public function deleting($model)
  {
    $decorator = new AncestryDecorator($model);
    $children  = $decorator->children()->get();
    foreach($children as $child) {
        $builder = new AncestryBuilder($child);
        $builder->root();
    }
  }
}